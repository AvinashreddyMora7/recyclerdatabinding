package com.example.gm.databindingwithrecyclerview;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;

import com.example.gm.databindingwithrecyclerview.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private FlightsRecyclerViewAdapter adapter;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.flightsRv.setLayoutManager(new LinearLayoutManager(this));
        binding.flightsRv.addItemDecoration(
        new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        FlightsRecyclerViewAdapter adapter =
                new FlightsRecyclerViewAdapter(prepareData(), this);
        binding.flightsRv.setAdapter(adapter);


    }
  public List<Flight> prepareData (){
        List<Flight> flights = new ArrayList<Flight>();
      Flight flight = new Flight("Delta", "Seattle", "London", "10:20", "17:30", "$388");
      flights.add(flight);
      flight = new Flight("Virgin Atlantic", "Seattle", "London", "10:20", "17:30", "$330");
      flights.add(flight);
      flight = new Flight("American Airlines", "Seattle", "London", "10:20", "17:30", "$400");
      flights.add(flight);
      flight = new Flight("British Airways", "Seattle", "London", "10:20", "17:30", "$440");
      flights.add(flight);
      flight = new Flight("Quatar Airways", "Seattle", "London", "10:20", "17:30", "$300");
      flights.add(flight);
      flight = new Flight("KLM", "Seattle", "London", "10:20", "17:30", "$350");
      flights.add(flight);
      flight = new Flight("Emirates", "Seattle", "London", "10:20", "17:30", "$420");
      flights.add(flight);
      flight = new Flight("Lufthansa","Seattle", "London", "10:20", "17:30", "$390");
      flights.add(flight);
      flight = new Flight("Air India", "Seattle", "London", "10:20", "17:30", "$350");
      flights.add(flight);
      flight = new Flight("Jet Airways", "Seattle", "London", "10:20", "17:30", "$390");
      flights.add(flight);
      flight = new Flight("United", "Seattle", "London", "10:20", "17:30", "$3450");
      flights.add(flight);
      flight = new Flight("Air Canada","Seattle", "London", "10:20", "17:30", "$398");
      flights.add(flight);

      return flights;
  }

}
