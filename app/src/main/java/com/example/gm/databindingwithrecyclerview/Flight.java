package com.example.gm.databindingwithrecyclerview;

import java.util.ArrayList;
import java.util.List;

public class Flight {

     private  String delta;
     private  String seattle;
     private String london;
     private  String seattleTime;
     private String londonTime;
     private String dollar;


    public Flight() {
    }

    public Flight(String delta, String seattle, String london, String seattleTime, String londonTime, String dollar) {
        this.delta = delta;
        this.seattle = seattle;
        this.london = london;
        this.seattleTime = seattleTime;
        this.londonTime = londonTime;
        this.dollar = dollar;
    }

    public String getDelta() {
        return delta;
    }

    public void setDelta(String delta) {
        this.delta = delta;
    }

    public String getSeattle() {
        return seattle;
    }

    public void setSeattle(String seattle) {
        this.seattle = seattle;
    }

    public String getLondon() {
        return london;
    }

    public void setLondon(String london) {
        this.london = london;
    }

    public String getSeattleTime() {
        return seattleTime;
    }

    public void setSeattleTime(String seattleTime) {
        this.seattleTime = seattleTime;
    }

    public String getLondonTime() {
        return londonTime;
    }

    public void setLondonTime(String londonTime) {
        this.londonTime = londonTime;
    }

    public String getDollar() {
        return dollar;
    }

    public void setDollar(String dollar) {
        this.dollar = dollar;
    }
}
