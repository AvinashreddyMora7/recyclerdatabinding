package com.example.gm.databindingwithrecyclerview;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.gm.databindingwithrecyclerview.databinding.ActivityMainBinding;
import com.example.gm.databindingwithrecyclerview.databinding.ItemBinding;

import java.util.ArrayList;
import java.util.List;

public class FlightsRecyclerViewAdapter extends RecyclerView.Adapter<FlightsRecyclerViewAdapter.ViewHolder>{

    private String TAG = "FlightsRecyclerViewAdapter";
    private List<Flight> flightsList;
    private Context context;


    public FlightsRecyclerViewAdapter(List<Flight> flsLst, Context ctx){
        flightsList = flsLst;
        context = ctx;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        ItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),R.layout.item,viewGroup,false);
        ViewHolder holder = new ViewHolder(binding);
        return holder;
    }



    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
       // Log.i(TAG,"onBindViewHolder() >> " + i);
        Toast.makeText(context,"bindholder"+i,Toast.LENGTH_LONG).show();
        Flight flight = flightsList.get(i);
        viewHolder.itemBinding.setFlight(flight);
       // viewHolder.itemBinding.setItemClickListner(this);

    }

    @Override
    public int getItemCount() {
        return flightsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public ItemBinding itemBinding;

        public ViewHolder(ItemBinding itemBinding) {
            super(itemBinding.getRoot());
            itemBinding = itemBinding;
        }
    }
  /* public void bookFlight(Flight f){
        Toast.makeText(context, "You booked "+f.getDelta(),
                Toast.LENGTH_LONG).show();
    }*/
}
